﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OutlineUI : MonoBehaviour 
{
    public Color hoverColor;
    public Color selectColor;

    private Outline outline;
    private bool selected;

	void Start () 
    {
        outline = GetComponent<Outline>();
	}
	
    public void Outline()
    {
        if (selected)
            return;

        if (outline.enabled)
        {
            outline.enabled = false;            
        }
        else
        {               
            outline.enabled = true;
            outline.effectColor = hoverColor;
        }        
    }

    public void Select()
    {
        outline.enabled = true;
        selected = true;
        outline.effectColor = selectColor;
    }

    public void Deselect()
    {
        outline.enabled = false;
        selected = false;
    }
}
