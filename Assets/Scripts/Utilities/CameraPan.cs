﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPan : MonoBehaviour 
{
    public float speed;
    public float minXPos;
    public float maxXPos;

    private GameController gameController;
    private Vector3 cameraStartPos;
    private Vector3 cameraPos;

    void Awake()
    {
        gameController = GameObject.Find("GameController").GetComponent<GameController>();
        cameraStartPos = transform.position;
    }

    void Update()
    {
        //if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        //{
        //    Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
        //    transform.Translate(-touchDeltaPosition.x * speed, -touchDeltaPosition.y * speed, 0);
        //}
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            cameraStartPos = transform.position;
        }
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved && !gameController.panelOpened)
        {
            if (Mathf.Abs(cameraStartPos.x - transform.position.x) > 2)
            {
                gameController.cameraIsMoving = true;
                    if(gameController.currentObject != null)
                gameController.currentObject.DeselectObject();
            }

            Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
            //transform.Translate(-touchDeltaPosition.x * speed * Time.deltaTime, -touchDeltaPosition.y * speed * Time.deltaTime, 0);
            transform.Translate(-touchDeltaPosition.x * speed * Time.deltaTime, transform.position.y * speed * Time.deltaTime, 0);

            Vector3 tmpPosX = transform.position;
            //tmpPosX.x = Mathf.Clamp(tmpPosX.x, -13.0f, 14.0f);
            tmpPosX.x = Mathf.Clamp(tmpPosX.x, minXPos, maxXPos);
            transform.position = tmpPosX;


            Vector3 tmpPosY = transform.position;
            tmpPosY.y = Mathf.Clamp(tmpPosY.y, -7.0f, 18.0f);
            //transform.position = tmpPosY;
        }
        if (Input.touchCount > 0 && (Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled))
        {
            if (gameController.cameraIsMoving)
                Invoke("EnableCameraMovement", 0.2f);                     
        }
    }

    void EnableCameraMovement()
    {
        if (!gameController.panelOpened)
        {
            gameController.cameraIsMoving = false;
            gameController.DeselectAllInteractables();   
        }
    }

}
