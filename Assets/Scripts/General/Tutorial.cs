﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour 
{
    private Animator choicePanelAnim;

	void Awake () 
    {
        choicePanelAnim = GameObject.Find("choicePanel").GetComponent<Animator>();
        Screen.orientation = ScreenOrientation.Portrait;
	}
	
    void Update()
    {
      
    }

    public void PresentChoice()
    {
        choicePanelAnim.SetTrigger("hide");
    }

    public void FutureChoice()
    {
        choicePanelAnim.SetTrigger("hide");
    }

}
