﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour {
    
    private SpriteOutline spOutline;
    private GameController gameController;
    private Sprite originalSprite;
    
    private SpriteRenderer sp;

    public GameController.PanelType objType;
    public Sprite alternativeSprite;
    public Vector3 doorTarget;

    public Sprite tipImage;
    public string tipTitle;
    [Multiline]
    public string tipText;

	void Awake () 
    {
        gameController = GameObject.Find("GameController").GetComponent<GameController>();
        spOutline = GetComponent<SpriteOutline>();
        spOutline.enabled = false;
        spOutline.color = gameController.outlineColor;
        sp = GetComponent<SpriteRenderer>();
        originalSprite = sp.sprite;
	}

    void OnMouseEnter()
    {
        if (gameController.panelOpened || gameController.cameraIsMoving)
            return;

        spOutline.enabled = true;
    }

    void OnMouseExit()
    {
        //if (gameController.panelOpened || gameController.cameraIsMoving)
        //    return;

        if (gameController.panelOpened)
            return;

        if (objType != GameController.PanelType.Door)
        {
            spOutline.enabled = false; 
        }
    }

    void OnMouseDown()
    {
        if (gameController.panelOpened || gameController.cameraIsMoving)
            return;        
    }

    void OnMouseUp()
    {
        if (gameController.panelOpened || gameController.cameraIsMoving)
            return;
                
        gameController.currentPanelType = objType;

        if (objType == GameController.PanelType.Door)
        {
            if (sp.sprite == alternativeSprite)
            {
                gameController.EnterRoom(doorTarget);
                sp.sprite = originalSprite;
            }
            else
            {
                gameController.DeselectAllInteractables();
                sp.sprite = alternativeSprite;
            }
                
            
        }
        else
        {
            gameController.panelOpened = true;
            if (gameController.currentObject != null && gameController.currentObject.name != this.gameObject.name)
            {
                gameController.DeselectAllInteractables();
            }
        }

        if (objType == GameController.PanelType.Tip || objType == GameController.PanelType.Confirmation)
        {
            gameController.currentMessageText = tipText;
            gameController.currentMessageTitle = tipTitle;
            gameController.currentMessageImage = tipImage;
        }
             
        gameController.currentObject = this;
        spOutline.enabled = true;
        gameController.ShowTipCommentPanel();
    }

    public void DeselectObject()
    {
        spOutline.enabled = false;
        sp.sprite = originalSprite;        
    }
}
