﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour 
{

    public enum PanelType
    {
        Tip,
        Comment,
        Door,
        Confirmation
    }

    public string currentMessageText = "";
    public Sprite currentMessageImage;
    public string currentMessageTitle = "";
    public GameController.PanelType currentPanelType;
    public Interactable currentObject;
    public Color outlineColor;
    public bool panelOpened = false;
    public bool cameraIsMoving = false;   

    private Animator tipPanelAnim;
    private Animator commentPanelAnim;
    private Animator tipConfirmAnim;
    private Image tipImage;
    private Text tipTitle;
    private Text tipText;
    private Image tipConfImage;
    private Text tipConfTitle;
    private Text tipConfText;
    private GameObject canvas;
    private List<GameObject> listInteractables;
    private List<Interactable> interactableObjs;

    void Awake()
    {
        Screen.orientation = ScreenOrientation.Landscape;

        tipPanelAnim = GameObject.Find("TipPanel").GetComponent<Animator>();
        commentPanelAnim = GameObject.Find("CommentPanel").GetComponent<Animator>();
        tipConfirmAnim = GameObject.Find("TipConfirm").GetComponent<Animator>();
        canvas = GameObject.Find("Canvas");        
        listInteractables = GameObject.FindGameObjectsWithTag("Interactable").ToList();
        interactableObjs = new List<Interactable>();
        tipImage = GameObject.Find("tipImage").GetComponent<Image>();
        tipText = GameObject.Find("tipText").GetComponent<Text>();
        tipTitle = GameObject.Find("tipTitle").GetComponent<Text>();
        tipConfImage = GameObject.Find("tipConfImage").GetComponent<Image>();
        tipConfText = GameObject.Find("tipConfText").GetComponent<Text>();
        tipConfTitle = GameObject.Find("tipConfTitle").GetComponent<Text>();

        foreach (GameObject go in listInteractables)
        {
            interactableObjs.Add(go.GetComponent<Interactable>());            
        }

        listInteractables.Clear();
        canvas.SetActive(false);
    }

    void Update()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            if (panelOpened)
                HideTipCommentPanel();
        }
    }

    public void ShowTipCommentPanel()
    {
        canvas.SetActive(true);        
        switch (currentPanelType)
        {            
            case PanelType.Tip:
                tipImage.sprite = currentMessageImage;
                tipText.text = currentMessageText;
                tipTitle.text = currentMessageTitle;
                tipPanelAnim.SetBool("show", true);
                panelOpened = true;
                break;
            case PanelType.Comment:
                commentPanelAnim.SetBool("show", true);
                panelOpened = true;
                break;
            case PanelType.Confirmation:
                tipConfImage.sprite = currentMessageImage;
                tipConfText.text = currentMessageText;
                tipConfTitle.text = currentMessageTitle;
                tipConfirmAnim.SetBool("show", true);
                panelOpened = true;
                break;
        }        
    }

    public void HideTipCommentPanel()
    {        
        switch (currentPanelType)
        {
            case PanelType.Tip:
                tipPanelAnim.SetBool("show", false);
                break;
            case PanelType.Comment:
                commentPanelAnim.SetBool("show", false);
                break;
            case PanelType.Confirmation:
                tipConfirmAnim.SetBool("show", false);
                break;
        }
        DeselectAllInteractables();
        //currentObject.DeselectObject();
        Invoke("EnableTouchInObjects", 0.2f);
    }

    void EnableTouchInObjects()
    {
        panelOpened = false;
    }

    public void FillTipCommentPanel(string text, PanelType panelType)
    {
        currentMessageText = text;
        currentPanelType = panelType;
    }

    public void EnterRoom(Vector3 camPos)
    {
        GameObject.Find("Main Camera").transform.position = camPos;
    }

    public void DeselectAllInteractables()
    {
        foreach (Interactable item in interactableObjs)
        {
            item.DeselectObject();
        }        
    }

    void Vibrate()
    {
        Handheld.Vibrate();        
    }

}
