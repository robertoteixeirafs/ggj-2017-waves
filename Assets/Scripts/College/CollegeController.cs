﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CollegeController : MonoBehaviour 
{

    public string selectedCollege;
    
    void Awake()
    {
        Screen.orientation = ScreenOrientation.Landscape;
    }

    public void ChooseCollege()
    {
        selectedCollege = EventSystem.current.currentSelectedGameObject.name;
    }

    public void SelectCollege()
    {
        if (selectedCollege == "Medicine")
        {
            Debug.Log("Radio Frequency: 89.1");
        }
        else
        {
            Debug.Log("Radio Frequency: 104.0");
        }                
    }

}
